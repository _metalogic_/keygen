package keygen

import (
	"strconv"
	"sync"
	"time"
)

// Keyseq is a stateful key generator to ensure monotonically increasing keys derived from the current time
type Keyseq struct {
	prev int64
}

var keys *Keyseq
var once sync.Once

// GetKeyseq returns a singleton key sequence
func GetKeyseq() *Keyseq {
	once.Do(func() {
		keys = &Keyseq{}
	})
	return keys
}

// Next returns the next key in a monitonically increasing sequence based on the current time
func (g *Keyseq) Next() []byte {
	next := time.Now().UnixNano()
	if next <= g.prev {
		next = g.prev + 1
	}
	g.prev = next
	return []byte(strconv.FormatInt(next, 10))
}
