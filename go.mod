module bitbucket.org/_metalogic_/keygen

go 1.15

require (
	github.com/dgraph-io/badger/v2 v2.0.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/kr/pretty v0.2.0 // indirect
)
