package keygen

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"sort"
	"testing"
	"time"

	badger "github.com/dgraph-io/badger/v2"
)

var testKeys = GetKeyseq()

type FakeRecord struct {
	Index     int // we also add a number to keep track of the ground-truth creation order
	Timestamp time.Time
	Nanosec   int64
	Key       []byte
}

func TestKeyGeneration(t *testing.T) {

	// reproducible entropy source
	entropy := rand.New(rand.NewSource(time.Unix(1000000, 0).UnixNano()))

	// generate fake log records that contain a ground-truth sorting order and a key
	var records []FakeRecord
	for i := 0; i < 1000000; i++ {
		now := time.Now()
		key := testKeys.Next()
		r := FakeRecord{Index: i, Timestamp: now, Key: key}
		records = append(records, r)
	}
	// shuffle records before inserting to ensure order of insertion is irrelevant
	// reproducible entropy source
	entropy.Shuffle(len(records), func(i, j int) {
		records[i], records[j] = records[j], records[i]
	})

	// open tempory test Badger database
	tstdb, err := ioutil.TempDir("/tmp", "badger")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(tstdb)
	opts := badger.DefaultOptions(tstdb)
	db, err := badger.Open(opts)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// open a database transaction
	txn := db.NewTransaction(true)

	for _, e := range records {
		// serialize the event payload (to JSON for simplicity)
		eSerial, err := json.Marshal(e)
		if err != nil {
			panic(err)
		}

		// add the insert operation to the transaction
		// and open a new transaction if this one is full
		err = txn.Set(e.Key, []byte(eSerial))
		if err == badger.ErrTxnTooBig {
			if err := txn.Commit(); err != nil {
				panic(err)
			}
			txn = db.NewTransaction(true)
			if err := txn.Set(e.Key, []byte(eSerial)); err != nil {
				panic(err)
			}
		}
		if err != nil && err != badger.ErrTxnTooBig {
			panic(err)
		}
	}

	// flush the transaction
	if err := txn.Commit(); err != nil {
		panic(err)
	}

	// fmt.Printf("Saved %d records\n", len(records))
	// for _, rec := range records {
	// 	fmt.Printf("  %+v\n", rec)
	// }

	// open a database transaction
	txn = db.NewTransaction(false)

	// validate badger iteration order is equal to original creation order
	var retrieved []FakeRecord
	if err := db.View(func(txn *badger.Txn) error {

		// create a Badger iterator with the default settings
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		// have the iterator walk the LMB tree
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			// k := item.Key()
			err := item.Value(func(val []byte) error {
				// deserialize the fake records and store them
				var des FakeRecord
				err = json.Unmarshal(val, &des)
				if err != nil {
					panic(err)
				}
				retrieved = append(retrieved, des)
				return nil
			})
			if err != nil {
				panic(err)
			}
		}
		it.Close()
		return nil
	}); err != nil {
		panic(err)
	}

	// flush the transaction
	if err := txn.Commit(); err != nil {
		panic(err)
	}

	// fmt.Printf("Retrieved %d records\n", len(retrieved))
	// for _, rec := range retrieved {
	// 	fmt.Printf("  %+v\n", rec)
	// }

	success := sort.SliceIsSorted(retrieved, func(i, j int) bool {
		return retrieved[i].Index < retrieved[j].Index
	})

	fmt.Println(success)

	fmt.Printf("Inserted: %d, Retrieved: %d\n", len(records), len(retrieved))
}
